import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TopStoriesComponent, HomePageComponent, MainContentComponent } from './components'

const routes: Routes = [
  { path: 'top-stories', component: TopStoriesComponent },
  { path: 'home', component: HomePageComponent },
  { path: ':country/:categoryId/:subcategoryId', component: MainContentComponent },
  { path: '', component: HomePageComponent, pathMatch: 'full' }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  declarations: []
})
export class AppRoutingModule { }
