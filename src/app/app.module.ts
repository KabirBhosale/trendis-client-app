import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { HttpClientModule  } from '@angular/common/http';
import { NguCarouselModule } from '@ngu/carousel';


import { AppComponent } from './app.component';
import { TopStoriesComponent, HomePageComponent, SideNavBarComponent,TopMenuBarComponent,MainContentComponent, PostCarouselComponent } from './components';
import { MenuProviderService , FeedCardsService,CountryProviderService} from './services';
import { AppRoutingModule } from './app-routing.module';


export function init_app(countryService: CountryProviderService) {
  return () => countryService.getAndSetLocateCountry();
}

@NgModule({
  declarations: [
    AppComponent,
    SideNavBarComponent,
    TopMenuBarComponent,
    MainContentComponent,
    TopStoriesComponent,
    HomePageComponent,
    PostCarouselComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    NguCarouselModule
  ],
  providers: [
    MenuProviderService,
    FeedCardsService,
    CountryProviderService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [CountryProviderService], multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
