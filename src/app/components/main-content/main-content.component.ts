import { Component, OnInit } from '@angular/core';
import { FeedCardsService } from "../../services/feed-provider/feed-cards.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit {
  selectedCategoryId: String;
  selectedSubCategoryId: String;
  selectedCountry: String;
  public feedCards;
  constructor(
    private apiFeedCard: FeedCardsService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(params => {
      this.selectedCategoryId = params.categoryId;
      this.selectedSubCategoryId = params.subcategoryId;
      this.onFeedCards(params)
    });
  }


  ngOnInit() {

  }

  onFeedCards(params) {
    console.log("Feed view Param",params);
    
    this.apiFeedCard.getFeedCards(params).subscribe(res => {
      // console.log("15 array",res);
      this.feedCards = res;
    });
  }

}
