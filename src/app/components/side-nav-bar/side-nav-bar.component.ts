import { Component, OnInit } from '@angular/core';
import { Router, RoutesRecognized } from "@angular/router";
import { MenuProviderService, CountryProviderService } from '../../services'

@Component({
  selector: 'app-side-nav-bar',
  templateUrl: './side-nav-bar.component.html',
  styleUrls: ['./side-nav-bar.component.css']
})
export class SideNavBarComponent implements OnInit {
  allMenuConfig: {};
  locateCountry:String;

  selectedCategoryId:String;
  selectedSubcategoryId:String;
  selectedMenuConfig:{};
  constructor(
    private router: Router,
    private menuService: MenuProviderService,
    private countryService:CountryProviderService
  ) {
    this.allMenuConfig = this.menuService.getAllConfig();
    this.locateCountry=this.countryService.getLocateCountry();
  }

  ngOnInit() {
    this.router.events.subscribe(val => {
      if (val instanceof RoutesRecognized) {
        console.log(val.state.root.firstChild.params);
        this.selectedCategoryId=val.state.root.firstChild.params.categoryId;
        this.selectedSubcategoryId=val.state.root.firstChild.params.subcategoryId;
        this.selectedMenuConfig=this.menuService.getCategory(this.selectedCategoryId);
      }
    });
  }

  setSelectedMenu(menuItem){
    console.log("selected menu",menuItem);
    
    this.menuService.setSelectedMenuConfig(menuItem);
  }

}
