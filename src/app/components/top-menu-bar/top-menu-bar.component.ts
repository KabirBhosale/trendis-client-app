import { Component, OnInit } from '@angular/core';
import { Router, RoutesRecognized } from "@angular/router";
import { MenuProviderService, CountryProviderService } from '../../services';

@Component({
  selector: 'app-top-menu-bar',
  templateUrl: './top-menu-bar.component.html',
  styleUrls: ['./top-menu-bar.component.css']
})
export class TopMenuBarComponent implements OnInit {

  selectedCategoryId:String;
  selectedSubcategoryId:String;
  selectedMenuConfig:{};
  constructor( 
    private router: Router,
    private menuService: MenuProviderService,
    private countryService:CountryProviderService) {

  }

  ngOnInit() {
    this.router.events.subscribe(val => {
      if (val instanceof RoutesRecognized) {
        console.log(val.state.root.firstChild.params);
        this.selectedCategoryId=val.state.root.firstChild.params.categoryId;
        this.selectedSubcategoryId=val.state.root.firstChild.params.subcategoryId;
        this.selectedMenuConfig=this.menuService.getCategory(this.selectedCategoryId);
      }
    });
  }

}
