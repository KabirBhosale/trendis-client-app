import { TestBed, inject } from '@angular/core/testing';

import { CountryProviderService } from './country-provider.service';

describe('CountryProviderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CountryProviderService]
    });
  });

  it('should be created', inject([CountryProviderService], (service: CountryProviderService) => {
    expect(service).toBeTruthy();
  }));
});
