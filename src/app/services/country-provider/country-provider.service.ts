import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class CountryProviderService {
  locateCountry:String;
  constructor(private http : HttpClient) {
    console.log("Country service constructor");
   }

   getAndSetLocateCountry(){
    return new Promise((resolve, reject) => {
      this.http.get("http://ipinfo.io/geo").subscribe(data=>{
        if(data){
          console.log(data)
          this.locateCountry=data['country'];
          resolve(this.locateCountry);
        }
      });
    });
   }

   getLocateCountry(){
     return this.locateCountry;
   }

}
