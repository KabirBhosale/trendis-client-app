import { TestBed, inject } from '@angular/core/testing';

import { FeedCardsService } from './feed-cards.service';

describe('FeedCardsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FeedCardsService]
    });
  });

  it('should be created', inject([FeedCardsService], (service: FeedCardsService) => {
    expect(service).toBeTruthy();
  }));
});
