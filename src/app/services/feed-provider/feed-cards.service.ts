import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { CountryProviderService } from "../country-provider/country-provider.service";
import { MenuProviderService } from "../menu-provider/menu-provider.service";
@Injectable()
export class FeedCardsService {

  baseUrl = "http://www.trendis.com/api/feeds/"
  constructor(
    private http: HttpClient,
    private countryApi: CountryProviderService,
    private menuAPi: MenuProviderService
  ) { }

  getFeedCards(params) {

    if (params.categoryId == "top") {
      let pageType = "top/highlights"
      let api = this.baseUrl + `${pageType}?country=${params.country}&media=link,photo,video&networks=all&page=0`;
      return this.http.get(api);
    }
    else {
      let pageType = "top";
      let api = this.baseUrl + `${pageType}?country=${params.country}&media=link,photo,video&category=${params.categoryId}&subcategory=${params.subcategoryId}&networks=all&page=0`;
      return this.http.get(api);
    }
  }
}
