import { Injectable } from '@angular/core';

@Injectable()
export class MenuProviderService {
  
  selectedMenuConfig: {};
  selectedSubMenuConfig: {};
  menuConfigs: {} = [
    {
      id: "top",
      name: "Top 10",
      subMenuConfigs: [{
        id: "settings",
        name: "Customize MyTrender"
      },{
        id: "sharepad",
        name: "Bookmarked"
      }]
    },
    {
      id: "humor",
      name: "Humor",
      subMenuConfigs: [{
        id: "all",
        name: "All"
      }, {
          id: "comics",
          name: "Comics"
        }, {
          id: "dailyhumor",
          name: "Daily Humor"
        }, {
          id: "gags",
          name: "Pranks"
        }]
    },
    {
      id: "sports",
      name: "Sports",
      subMenuConfigs: [{
        id: "all",
        name: "All"
      }, {
          id: "american_football",
          name: "American Football"
        }, {
          id: "basketball",
          name: "Basketball"
        }, {
          id: "cricket",
          name: "Cricket"
        }, {
          id: "fights",
          name: "Fights"
        }, {
          id: "soccer",
          name: "Soccer"
        }, {
          id: "baseball",
          name: "Baseball"
        }, {
          id: "golf",
          name: "Golf"
        }, {
          id: "tennis",
          name: "Tennis"
        }]
    }, {
      id: "entertainment",
      name: "Entertainment",
      subMenuConfigs: [{
        id: "all",
        name: "All"
      }, {
          id: "movies",
          name: "Movies"
        }, {
          id: "music",
          name: "Music"
        }, {
          id: "celebrity",
          name: "Celebrity"
        }]
    }, {
      id: "infotainment",
      name: "Infotainment",
      subMenuConfigs: [{
        id: "all",
        name: "All"
      }, {
          id: "interesting",
          name: "Interesting"
        }, {
          id: "auto",
          name: "Auto"
        }, {
          id: "tech",
          name: "Tech"
        }, {
          id: "dailynews",
          name: "Daily News"
        }]
    }, {
      id: "lifestyle",
      name: "LifeStyle",
      subMenuConfigs: [{
        id: "all",
        name: "All"
      }, {
          id: "beauty",
          name: "Beauty"
        }, {
          id: "food",
          name: "Food"
        }, {
          id: "health",
          name: "Health"
        }, {
          id: "travel",
          name: "Travel"
        }]
    }, {
      id: "hobbies",
      name: "Hobbies",
      subMenuConfigs: [{
        id: "all",
        name: "All"
      }, {
          id: "books",
          name: "Books"
        }, {
          id: "diy",
          name: "DIY"
        }, {
          id: "photography",
          name: "Photography"
        }]
    }
  ]
  constructor(
    
  ) {
    console.log("Menu Provider Service contructor");
  }

  getAllConfig() {
    return this.menuConfigs;
  }

  setSelectedMenuConfig(config) {
    
    this.selectedMenuConfig = config;
    
  }

  getSelectedMenuConfig() {
    return this.selectedMenuConfig;
  }

  getCategory(categoryId){
    let category;
    for(let index in this.menuConfigs){
      if(this.menuConfigs[index]['id']==categoryId){
        category=this.menuConfigs[index];
      }
    }
    return category;
  }

  setSelectedSubMenuConfig(subConfig) {
    this.selectedSubMenuConfig = subConfig;
  }

  getSelectedSubMenuConfig() {
    return this.selectedSubMenuConfig;
  }
}
